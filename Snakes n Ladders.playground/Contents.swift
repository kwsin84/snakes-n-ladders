//: [Previous](@previous)

import Foundation

let numberOfPlayer: Int = 4
var board: [Int] = []
var players : [Int] = [Int](count: numberOfPlayer, repeatedValue: 0)
var hasWinner: Bool = false




func boardSetup(){
    var initialPlayerPosition: Int = 0
    while initialPlayerPosition < 100{
        
        board.append(0)
        initialPlayerPosition = initialPlayerPosition + 1
        
        
    }
    // add board
    board[03] = +3; board[10] = -5; board[20] = -10; board[30] = +5; board[50] = -15
    
}

func rollDice()->Int{
    
    let randomDiceNumber = Int(arc4random_uniform(6) + 1)
    return randomDiceNumber
    
    
}

boardSetup()

var round: Int = 0

while (hasWinner == false){
    
    
    round = round + 1
    print("Round number \(round)")
    for index in 1 ... numberOfPlayer{
        
        //var playerPosition = players[index - 1]
        
        if players[index - 1] > 99 {
            
            hasWinner = true
            
            print("The Winner is player \(index)")
            break
        } else {
            
            players[index - 1] = players[index - 1] + rollDice()
            
            if players[index - 1] < 100{
                var bonusMove = board[players[index - 1]]
                players[index - 1] += bonusMove
                
            }
            
            
        }
        
    }
    
    
}
//: [Next](@next)
